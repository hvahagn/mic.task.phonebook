﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Task.PhoneBook
{
    class Contact
    {
        public string Name { get; set; }



        public string Surname { get; set; }

        public string Gender { get; set; }
        public string PhoneNumber { get; set; }
        public int Code { get; set; }
        public string Profession { get; set; }
        public string Email { get; set; }
        public string Regions { get; set; }
        public string Country { get; set; }

        public int Number { get; set; }
        public string ContacFullData => $"{PhoneNumber}-{Name}-{Surname}-{Profession}-{Email}-{Gender}-{Regions}";

       
        public override string ToString()
        {
            return PhoneNumber;
        }
    }
}

