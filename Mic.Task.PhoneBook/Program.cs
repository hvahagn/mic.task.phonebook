﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Task.PhoneBook
{
    class Program
    {
        static void Main(string[] args)
        {

            var phoneDic = Generator.CreatePhonebook(50);

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.SetCursorPosition(55, 4);
            Console.WriteLine("Hello, user!");
            Console.SetCursorPosition(40, 7);
            Console.WriteLine("1) If You Want To Open Phonebook Please Enter '1'");
            Console.SetCursorPosition(40, 8);
            Console.WriteLine("2) For Search By Number Please Enter '2'");
            Console.SetCursorPosition(40, 9);
            Console.WriteLine("3) For Search By Operator Please Enter '3'");
            Console.SetCursorPosition(40, 10);
            Console.WriteLine("4) For Search By Gender Please Enter '4'");
            Console.SetCursorPosition(40, 11);
            Console.WriteLine("5) For Search By Region Please Enter '5'");
            Console.SetCursorPosition(40, 12);
            Console.WriteLine("6) For Search By Proffesion Please Enter '6'");
            int choose;
            while (true)

            {
                choose = Convert.ToInt32(Console.ReadLine());
                switch (choose)
                {
                    case 1:
                        Console.WriteLine("You have choosen show all list");
                        PhoneBook.PrintBook(phoneDic);
                        break;
                    case 2:
                        Console.WriteLine("You have choosen search by number. Please enter a number..");
                        int number = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine($"***               Filter By Number {number}***");
                        PhoneBook.SearchByNumber(phoneDic, number);
                        break;
                    case 3:
                        Console.WriteLine("You have choosen search by operator version. Please enter prefer operator name");
                        string opName = Console.ReadLine();
                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.WriteLine($"***               Filter By Operator {opName}***");
                        Console.WriteLine("\tPhone Number\t Name  Surname\t Profession\t E-mail\t Gender\t Region" + "\n");
                        PhoneBook.SearchByOperator(phoneDic, opName);
                        Console.ResetColor();
                        break;
                    case 4:
                        Console.WriteLine("You have choosen search by gender version. Please enter prefer gender");
                        string gender = Console.ReadLine();
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"                   ***Filter By Gender {gender}****");
                        Console.WriteLine("\tPhone Number\t Name  Surname\t Profession\t E-mail\t Gender\t Region" + "\n");                   
                        PhoneBook.SearchByGender(phoneDic, gender);
                        Console.ResetColor();
                        break;
                    case 5:
                        Console.WriteLine("You have choosen search by region version. Please enter prefer region name");
                        string region = Console.ReadLine();
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine($"                 ***Filter By Region {region}***");
                        PhoneBook.SearchByRegion(phoneDic, region);
                        Console.ResetColor();

                        break;
                    case 6:
                        Console.WriteLine("You have choosen search by profession version. Please enter prefer profession");
                        string profession = Console.ReadLine();
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"                     ***Filter By {profession} Profession****");
                        PhoneBook.SearchByProffesion(phoneDic, profession);
                        Console.ResetColor();
                        break;
                    default:
                        Console.WriteLine("You dont choose offered any version. Please try again!");
                        break;
                }


            }
            Console.ReadLine();



        }

    }
}
    


